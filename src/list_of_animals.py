adresses = [
    {'city': 'Wroclaw', 'street': 'Krzywa', 'house_number': 2, 'post_code': '50-361'},
    {'city': 'Krakow', 'street': 'Nowa', 'house_number': 5, 'post_code': '51-525'},
    {'city': 'Warszawa', 'street': 'Czarna', 'house_number': 11, 'post_code': '53-976'}
]

print(adresses[2]["post_code"])
print(adresses[1]["city"])

adresses[0]['street'] = 'nowa'

print(adresses)
