first_name = "Wojtek"
last_name = "Janusz"
age = 29

print(first_name)
print(last_name)
print(age)

friends_name = "Kamila"
age_in_years = 25
number_of_animals = 1
driver_license = False
length_of_acquaintance = 5.5

print(friends_name, age_in_years, number_of_animals, driver_license, length_of_acquaintance, sep=',')
